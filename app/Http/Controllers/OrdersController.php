<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    public function order(Request $request)
    {
        $productIds = $request->get("product_ids", []);

        $products = Product::query()->findMany($productIds);

        $order = Order::create([]);

        $order->products()->sync($products);

        return [
            "message" => "ok"
        ];
    }

    public function pay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'amount' => 'required|numeric',
        ]);

        if (!$validator->passes()) {
            return $validator->errors()->all();
        }

        $order = Order::query()->where([
            'id'=> $request->get("order_id"),
            'status' => 'new'
        ])->first();

        if (!$order) {
            return ["Order already paid."];
        }

        if ($request->get("amount") == $order->price()) {
            $client = new Client();

            $response = $client->request("GET", "https://ya.ru");

            if ($response->getStatusCode() == 200) {
                $order->pay();
                return ["ok"];
            }

            return ["Error"];
        }

        return [
            "Amount are not match.", $order->price()
        ];
    }
}
