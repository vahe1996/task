<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function generate()
    {
        Product::generate();

        return [
            "message" => "Products generated successful."
        ];
    }

    public function get()
    {
        return [
            "products" => Product::query()->select("id", "name", "price")->get()
        ];
    }
}
