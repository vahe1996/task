<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['status'];

    public function products()
    {
        return $this->belongsToMany(Product::class,
            'orders_products',
            'order_id',
            'product_id'
        );
    }

    public function pay()
    {
        $this->status = 'paid';
        $this->save();
    }

    public function price()
    {
        return round($this->products()->sum("price"), 2);
    }
}
