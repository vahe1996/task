<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price'];

    public static function generate()
    {
        $count = self::all()->count();
        for ($i = $count + 1; $i <= 20 + $count; $i++) {
            self::create([
                "name" => "Prod-" . $i,
                "price" => rand(1000, 10000) / 100
            ]);
        }
    }
}
